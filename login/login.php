<!DOCTYPE html>

<?php
session_start();

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
  header("Location: ../chat.php");  
}

if (! empty($_POST["login"])) {
  $username = filter_var($_POST["username"], FILTER_SANITIZE_STRING);
  $password = filter_var($_POST["password"], FILTER_SANITIZE_STRING);
  
  require_once ("../src/login/LoginService.php");
  $loginService = new LoginService();
  $errorMessage = $loginService->validateFields($username, $password);

  if (empty($errorMessage)) {
      $logInValid = $loginService->login($username, $password);
      
      if ($logInValid) {
        header("Location: ../chat.php");  
      } else {
        $errorMessage["global"] = "Benutzer oder Passwort sind falsch";
      }
  }
}


?>

<html>
    <head>
        <meta charset="UTF-8"> 
        <link rel="stylesheet" type="text/css" href="../css/login.css">
    </head>

    <body>
        <div class="log-form">
            <div class="log-block">
              <h3 class="logo">Chat4Us</h3>
              <span class="error"><?php if(! empty($errorMessage) && is_array($errorMessage) && array_key_exists("global", $errorMessage)) echo  $errorMessage["global"] ?></span>
              <h4>Anmelden</h4>
              <form action="" method="POST">
                <p>
                  <input type="text" name="username" title="username" placeholder="Benutzername / E-Mail" value="<?php if(isset($_POST['username'])) echo $_POST['username']; ?>"/>
                  <label for="username" class="error"><?php if(! empty($errorMessage) && is_array($errorMessage) && array_key_exists("username", $errorMessage)) echo  $errorMessage["username"] ?></label>
                </p>
                <p>
                  <input type="password" name="password" title="password" placeholder="Passwort" value="<?php if(isset($_POST['password'])) echo $_POST['password']; ?>"/>
                  <label for="password" class="error"><?php if(! empty($errorMessage) && is_array($errorMessage) && array_key_exists("password", $errorMessage)) echo  $errorMessage["password"] ?></label>
                  <br/>
                  <a class="forgot" href="./forgot.php">Password vergessen ?</a>
                </p>
                <button type="submit" name="login" value="login" class="btn btn-primary">Login</button>
                <a href="./registrieren.php" class="btn spacing ln">Registrieren</a>
                
              </form>
            </div>
        </div>
    </body>
</html>