<!DOCTYPE html>
<?php

if (! empty($_POST["register"])) {
  $email = filter_var($_POST["email"], FILTER_SANITIZE_STRING);
  $username = filter_var($_POST["username"], FILTER_SANITIZE_STRING);
  $password = filter_var($_POST["password"], FILTER_SANITIZE_STRING);
  $password_confirm = filter_var($_POST["password_confirm"], FILTER_SANITIZE_STRING);
  
  require_once ("../src/login/RegistrationService.php");
  $registrationService = new RegistrationService();
  $errorMessage = $registrationService->validateFields($email, $username, $password, $password_confirm);

  if (empty($errorMessage)) {
      $count = $registrationService->isUserExistent($username, $email);
      
      if ($count == 0) {
          $insertId = $registrationService->register($email, $username, $password);
          if (empty($insertId)) {
              header("Location: thank-you.html");
          }
      } else {
          $errorMessage["global"] = "Der Benutzer existiert bereits.";
      }
  }
}



?>


<html>
    <head>
        <meta charset="UTF-8"> 
        <link rel="stylesheet" type="text/css" href="../css/login.css">
    </head>

    <body>
        <div class="log-form">
            <div class="log-block">
              <span class="error"><?php if(! empty($errorMessage) && is_array($errorMessage) && array_key_exists("global", $errorMessage)) echo  $errorMessage["global"] ?></span>
              <h4>Registrieren</h4>
              <form name="frmRegistration" method="post" action="">
                <p>
                  <label for="email">E-Mail</label>
                  <input type="text" title="email" name="email" value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>" />
                  <label for="email" class="error"><?php if(! empty($errorMessage) && is_array($errorMessage) && array_key_exists("email", $errorMessage)) echo  $errorMessage["email"] ?></label>
                </p>
                <p>
                  <label for="username">Anzeigename</label>
                  <input type="text" title="username" name="username" value="<?php if(isset($_POST['username'])) echo $_POST['username']; ?>" />
                  <label for="username" class="error"><?php if(! empty($errorMessage) && is_array($errorMessage) && array_key_exists("username", $errorMessage)) echo  $errorMessage["username"] ?></label>
                </p>
                <p>
                  <label for="password">Passwort</label>
                  <input type="password" title="password" name="password" value="<?php if(isset($_POST['password'])) echo $_POST['password']; ?>"/>
                  <label for="password" class="error"><?php if(! empty($errorMessage) && is_array($errorMessage) && array_key_exists("password", $errorMessage)) echo  $errorMessage["password"] ?></label>
                </p>
                <p>
                  <label for="password_confirm">Passwort bestätigen</label>
                  <input type="password" title="password_confirm" name="password_confirm" value="<?php if(isset($_POST['password_confirm'])) echo $_POST['password_confirm']; ?>"/>
                  <label for="password_confirm" class="error"><?php if(! empty($errorMessage) && is_array($errorMessage) && array_key_exists("password_confirm", $errorMessage)) echo  $errorMessage["password_confirm"] ?></label>
                </p>
                <p>
                  <input type="checkbox" name="terms"> Ich akzeptiere die Geschäftsbedingungen</input>
                  <label for="terms" class="error"><?php if(! empty($errorMessage) && is_array($errorMessage) && array_key_exists("terms", $errorMessage)) echo  $errorMessage["terms"] ?></label>
                </p>
                <button type="submit" value="register" name="register" class="btn btn-primary">Registrieren</button>
                
              </form>
            </div>
        </div>
    </body>
</html>