<!DOCTYPE html>

<?php
if (! empty($_POST["password_reset"])) {
  $username = filter_var($_POST["username"], FILTER_SANITIZE_STRING);
  $password = filter_var($_POST["password"], FILTER_SANITIZE_STRING);
  
  require_once ("../src/login/ForgotService.php");
  $forgotService = new ForgotService();
  $errorMessage = $forgotService->validateFields($username, $password);

  if (empty($errorMessage)) {
      $logInValid = $forgotService->forgotPassword($username, $password);
      
      if ($logInValid) {
        header("Location: ../chat.php");  
      } else {
        $errorMessage["global"] = "Der Benutzer konnte nicht gefunden werden.";
      }
  }
}
?>

<html>
    <head>
        <meta charset="UTF-8"> 
        <link rel="stylesheet" type="text/css" href="../css/login.css">
    </head>

    <body>
        <div class="log-form">
            <div class="log-block">
              <h3 class="logo">Chat4Us</h3>
              <span class="error"><?php if(! empty($errorMessage) && is_array($errorMessage) && array_key_exists("global", $errorMessage)) echo  $errorMessage["global"] ?></span>
              <h4>Passwort vergessen</h4>
              <form action="" method="POST">
                <p>
                  <input type="text" name="username" title="username" placeholder="Benutzername / E-Mail" value="<?php if(isset($_POST['username'])) echo $_POST['username']; ?>"/>
                  <label for="username" class="error"><?php if(! empty($errorMessage) && is_array($errorMessage) && array_key_exists("username", $errorMessage)) echo  $errorMessage["username"] ?></label>
                </p>
                <p>
                  <input type="password" name="password" title="password" placeholder="Passwort" value="<?php if(isset($_POST['password'])) echo $_POST['password']; ?>"/>
                  <label for="password" class="error"><?php if(! empty($errorMessage) && is_array($errorMessage) && array_key_exists("password", $errorMessage)) echo  $errorMessage["password"] ?></label>
                  <br/>
                </p>
                <button type="submit" name="password_reset" value="password_reset" class="btn btn-primary">Passwort erneuern</button>
              </form>
            </div>
        </div>
    </body>
</html>