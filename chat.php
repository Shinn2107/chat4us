<!DOCTYPE html>
<?php
    session_start();
    
    if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] == false) {
      header("Location: login/login.php");  
    }
?>

<html>
    <head>
        <meta charset="UTF-8"> 
        <link rel="stylesheet" type="text/css" href="css/chat.css">
    </head>

    <script>  

        var websocket;
        (function() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    let response = this.responseText;
                    initResponse = JSON.parse(response);
                    websocket = new WebSocket("ws://localhost:8090/chat4us/websocket/chat-socket.php"); 
                    websocket.onopen = function(event) { 
                        showMessage({message:"Verbindung hergestellt",messageType:"chat-connection-ack"});
                        setInterval(pingPong,10000)
                        showLatestMessages(initResponse.channels)	
                    }
                    websocket.onmessage = function(event) {
                        var payload = JSON.parse(event.data);

                        switch(payload.messageType){
                            case "message":
                                showMessage(payload);
                                break;
                            case "chat-connection-ack":
                                showMessage(payload);
                                break;
                        }
                        document.getElementById("message").value = "";

                    };
                    
                    websocket.onerror = function(event){
                        showMessage({message:"Ein Problem mit der Verbindung ist aufgetreten",messageType:"error"});
                    };

                    websocket.onclose = function(event){
                        showMessage({message:"Verbindung geschlossen, versuchen Sie die Seite neuzuladen",messageType:"chat-connection-ack"});
                        sendMessage("logout")
                    }; 
                }
            };
            xhttp.open("GET", "src/chat/ws/init.php", true);
            xhttp.setRequestHeader("Content-type", "application/json");
            xhttp.send();
        })();

        function showMessage(payload) {
            const chatDiv = document.createElement('div');
            chatDiv.className=payload.messageType;
            if(payload.sender == "<?php echo $_SESSION['username']?>"){
                chatDiv.className+= " darker";
            }
            if(payload.sender && payload.time){
                const chatHeader = document.createElement('p')
                chatHeader.className='chat-box-message';
                chatHeader.innerHTML= payload.sender+" ("+payload.time+"): "
                chatDiv.append(chatHeader);
            }
            
            const chatMessage = document.createElement('div');
            chatMessage.className='chat-box-message';
            chatMessage.innerHTML = payload.message;
            chatDiv.append(chatMessage);
            document.getElementById("chat").append(chatDiv);
            document.querySelector("#chat").scrollTop = document.querySelector("#chat").scrollHeight
        }

        function showLatestMessages(channels) {
            for(var i = 0; i<channels.length;i++){
                for(var j=0; j<channels[i].messages.length;j++){
                    showMessage(channels[i].messages[j]);
                }
                break; //Only show one channel for now should be extended when more channels are in
            }
        }

        function pingPong(){
            websocket.send(JSON.stringify({messageType:"ping"}))
        }

        function sendMessage(type = "message"){
            if(websocket){
                const websocketMessage = {
                    message : "",
                    messageType: type,
                    channel: "1"
                }
                switch(type){
                    case "message":
                        let textMessage = document.getElementById("message").value
                        websocketMessage.message = textMessage
                        var xhttp = new XMLHttpRequest();
                        xhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                websocket.send(this.responseText);
                            }
                        };
                        xhttp.open("POST", "src/chat/ws/sendMessage.php", true);
                        xhttp.setRequestHeader("Content-type", "application/json");
                        xhttp.send(JSON.stringify(websocketMessage)); 
                        break;
                        
                }
                
            } 
        }      

        function logout(){
            console.log("hellO")
            window.location ="login/logout.php";
        }
         
        </script>
    <body>
        <nav>
            <span class="title">Chat4Us</span>
            <div class="navigation-content">
                Willkommen <?php echo $_SESSION['username']?>
                    <button type="button" onclick="logout()" name="login" value="login" class="btn">Ausloggen</button>
            </div>
        
        </nav>
        <div class="content">
            <!--<div id="users" class="content_flex">
                <div class="channel">Welt-Chat</div>
            </div>-->
            <div id="chat" class="content_flex"></div>
            
        </div>
        <div class="control">
            <form onsubmit="event.preventDefault(); sendMessage();">
                <input type="text" placeholder="Deine Nachricht" name="message" id="message"/>
                <button type="button" class="btn btn-primary" onclick="sendMessage()">Senden</button>
            </form>        
        </div>
        
    </body>
</html>