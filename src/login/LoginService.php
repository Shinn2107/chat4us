<?php

class LoginService{

    private $datatabaseService;

    function __construct()
    {
        require_once "../src/database/DatabaseService.php";
        $this->datatabaseService = new DatabaseService();
    }


    function login($username, $password)
    {
        $query = "select * FROM Users WHERE username = ? OR email = ?";
        $paramType = "ss";
        $paramArray = array($username, $username);
        $resultset = $this->datatabaseService->select($query, $paramType, $paramArray);


        if(empty($resultset) || count($resultset) > 1){
            return false;
        }
     
        $user = $resultset[0];

        if(password_verify($password,$user["password"])){
            
            $_SESSION['loggedin'] = true;
            $_SESSION['username'] = $user["username"];
            $_SESSION['email'] = $user["email"];
            $query = "UPDATE Users set sessionId=?,lastLogin=? where email=?";
            $paramType = "sss";
            $paramArray = array(
                session_id(),
                date('Y-m-d H:i:s'),
                $user["email"], 
            );
            $updateId = $this->datatabaseService->insert($query, $paramType, $paramArray);
            return true;
        }
        
        return false;
    }

    function validateFields(){
        $valid = TRUE;
        $errorMessage = array();
        foreach ($_POST as $key => $value) {
            if (empty($_POST[$key])) {
                $valid = false;
                $errorMessage[$key] = "Das Feld muss gefüllt werden";
            }
        }
        return $errorMessage;
       
    
    }

}



?>