<?php



class RegistrationService{

    private $datatabaseService;

    function __construct()
    {
        require_once "../src/database/DatabaseService.php";
        $this->datatabaseService = new DatabaseService();
    }

    function validateFields(){
        $valid = TRUE;
        $errorMessage = array();
        foreach ($_POST as $key => $value) {
            if (empty($_POST[$key])) {
                $valid = false;
                $errorMessage[$key] = "Das Feld muss gefüllt werden";
            }
        }
        if($valid) {
         
            if ($_POST['password'] != $_POST['password_confirm']) {
                $errorMessage["password"] = 'Passwörter müssen gleich sein !';
           
            }
            
            if (! filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
                $errorMessage["email"] = "Ungültige E-Mail addresse";
            
            }
            
            if (!isset($_POST["terms"])) {
                $errorMessage["terms"] = "Bitte akzeptieren Sie die Geschäftsbedingungen!";

            }
            
        }
        return $errorMessage;
       
    
    }

    function isUserExistent($username, $email)
    {
        $query = "select * FROM Users WHERE username = ? OR email = ?";
        $paramType = "ss";
        $paramArray = array($username, $email);
        $userCount = $this->datatabaseService->numRows($query, $paramType, $paramArray);
        
        return $userCount;
    }

    function register($email, $username, $password)
    {
        $password_hashed = password_hash($password, PASSWORD_DEFAULT);
        $query = "INSERT INTO Users(username, email, password) VALUES (?, ?, ?)";
        $paramType = "sss";
        $paramArray = array(
            $username,
            $email,
            $password_hashed
        );
        $insertId = $this->datatabaseService->insert($query, $paramType, $paramArray);

        $this->insertIntoGeneralChannel($email);
        return $insertId;
    }


    private function insertIntoGeneralChannel($email){
        $query = "INSERT INTO UserToChannel(user, channelId) VALUES (?, ?)";
        $paramType = "si";
        $paramArray = array(
            $email,
            1
        );
        $insertId = $this->datatabaseService->insert($query, $paramType, $paramArray);
    }
}




?>