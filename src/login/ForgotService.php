<?php

class ForgotService{

    private $datatabaseService;

    function __construct()
    {
        require_once "../src/database/DatabaseService.php";
        $this->datatabaseService = new DatabaseService();
    }


    function forgotPassword($username, $password)
    {
        $query = "select * FROM Users WHERE username = ? OR email = ?";
        $paramType = "ss";
        $paramArray = array($username, $username);
        $resultset = $this->datatabaseService->select($query, $paramType, $paramArray);
        if(empty($resultset) || count($resultset) > 1){
            return false;
        }
        $user = $resultset[0];
        $password_hashed = password_hash($password, PASSWORD_DEFAULT);
        $query = "UPDATE Users set password=? WHERE username = ? OR email = ?";
        $paramType = "sss";
        $paramArray = array(
            $password_hashed,
            $username,
            $username,
        );
        $insertId = $this->datatabaseService->insert($query, $paramType, $paramArray);        
        return true;
    }

    function validateFields(){
        $valid = TRUE;
        $errorMessage = array();
        foreach ($_POST as $key => $value) {
            if (empty($_POST[$key])) {
                $valid = false;
                $errorMessage[$key] = "Das Feld muss gefüllt werden";
            }
        }
        return $errorMessage;
       
    
    }

}



?>