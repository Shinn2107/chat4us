<?php


class ChatService{
    private $datatabaseService;

    function __construct()
    {
        require_once "../../database/DatabaseService.php";
        require_once "../WebsocketMessage.php";
        require "../Channel.php";
        $this->datatabaseService = new DatabaseService();
    }


    function isUserLoggedIn(){
        return isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true;
    }


    function sendMessage(WebsocketMessage $payload){

        if($this->isUserInChannel($payload->channel)){
            $query = "INSERT INTO Message(user,channelId,message,createdTime) VALUES (?, ?, ? , ?)";
            $paramType = "siss";
            $paramArray = array(
                $_SESSION['email'],
                intval($payload->channel),
                $payload->message,
                $payload->time
                

            );
            $insertId = $this->datatabaseService->insert($query, $paramType, $paramArray);
            return true;
        }
        return false;
    }

    function initUserInformation(){
        $initMessage = new WebsocketInitMessage();
        $initMessage->user = $_SESSION['email'];
        $query = "select * FROM UserToChannel as utc join Channel as ch on utc.channelId=ch.id  WHERE user = ?";
        $paramType = "s";
        $paramArray = array($initMessage->user);
        $resultset = $this->datatabaseService->select($query, $paramType, $paramArray);
        if(empty($resultset) || count($resultset) > 1){
            return false;
        }
        $channels = array();
        foreach($resultset as $row){
            $ch = new Channel();
            $ch->id = $row["channelId"];
            $ch->name = $row["name"];
            $query = "select * FROM Message as m join Users as us on m.user=us.email WHERE channelId=? AND createdTime > ? ORDER BY createdTime ASC LIMIT 30"; 
            $paramType = "is";
            $paramArray = array($ch->id, date("Y-m-d H:i:s", time() - 60 * 60 * 24));
            $messageResults = $this->datatabaseService->select($query, $paramType, $paramArray);
            foreach($messageResults as $messageResultRow){
                $message = new WebsocketMessage();
                $message->message = $messageResultRow["message"];
                $message->messageType = "message";
                $message->sender = $messageResultRow["username"];
                $message->time = $messageResultRow["createdTime"];
                $message->channel = $ch->id;
                array_push($ch->messages,$message);
            }
            array_push($channels,$ch);
        }
        $initMessage->channels = $channels;
        $initMessage->messageType = "init";
        return $initMessage;
    }


    private function isUserInChannel($channel){
        $query = "select * FROM UserToChannel WHERE user = ? AND channelId=?";
        $paramType = "ss";
        $paramArray = array($_SESSION['email'],$channel);
        $resultset = $this->datatabaseService->select($query, $paramType, $paramArray);
        if(empty($resultset) || count($resultset) > 1){
            return false;
        }
        return true;
    }



}


?>