<?php
session_start();
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  

require_once("../service/ChatService.php");
require_once("../ChatHash.php");
require("../WebsocketInitMessage.php");
require("../ErrorMessage.php");

$requestMethod = $_SERVER["REQUEST_METHOD"];
if($requestMethod!="GET"){
    http_response_code("405");
    $errorMessage->code=405;
    $errorMessage->message="Method not allowed";
    echo json_encode($errorMessage);
}else{
    $chatService = new ChatService();
    $chatHash = new ChatHash();
    $errorMessage = new ErrorMessage();
    if(!$chatService->isUserLoggedIn()){
        http_response_code("403");
        $errorMessage->code=403;
        $errorMessage->message="Forbidden";
        echo(json_encode($errorMessage));
    }else{
        $message = $chatService->initUserInformation();
        $message->messageHash = $chatHash->hashMessage($message);
        if(!empty($message->channels)){
            echo(json_encode($message));
        }else{
            http_response_code(409);
            $errorMessage->code=409;
            $errorMessage->message="There was a problem with the message, likely it was tempted with.";
            echo(json_encode($errorMessage));
        }
        
    }
}


?>