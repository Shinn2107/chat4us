<?php
session_start();
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  

require_once("../service/ChatService.php");
require_once("../ChatHash.php");
require("../WebsocketMessage.php");
require("../ErrorMessage.php");




$requestMethod = $_SERVER["REQUEST_METHOD"];
if($requestMethod!="POST"){
    http_response_code("405");
    $errorMessage->code=405;
    $errorMessage->message="Method not allowed";
    echo json_encode($errorMessage);
}else{
    $chatService = new ChatService();
    $chatHash = new ChatHash();
    $errorMessage = new ErrorMessage();
    if(!$chatService->isUserLoggedIn()){
        http_response_code("403");
        $errorMessage->code=403;
        $errorMessage->message="Forbidden";
        echo(json_encode($errorMessage));
    }else{
    
        $payload = json_decode(file_get_contents("php://input"));
        $message = new WebsocketMessage();
        $message->message = $payload->message;
        $message->messageType = $payload->messageType;
        $message->sender = $_SESSION['username'];
        $message->time = date('Y-m-d H:i:s');
        $message->channel = $payload->channel;
        $message->session = session_id();
        $message->messageHash = $chatHash->hashMessage($message);
        try{
            $success = $chatService->sendMessage($message);
            if($success){
                echo(json_encode($message));
            }else{
                http_response_code(409);
                $errorMessage->code=409;
                $errorMessage->message="There was a problem with the message, likely it was tempted with.";
                echo(json_encode($errorMessage));
            }
        }catch(Exception $e){
            http_response_code(500);
            $errorMessage->code=500;
            $errorMessage->message="Internal Server Error";
            echo(json_encode($errorMessage));
        }
    }
}


?>