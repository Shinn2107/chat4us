<?php

class ChatHash{

    private $SALT = "CHAT4US";

    public function hashMessage($message){
        $array = (array) $message;
        sort($array);
        $toHash= "";
        foreach($array as $key => $value){
            if(!is_array($value)&&$key!="messageHash"){
                $toHash = $toHash.$value.".";
            }
        }

        return hash("sha256",$this->SALT.".".$toHash.$this->SALT);

    }

    public function isHashValid($message, $hashToCompare){
        return $this->hashMessage($message) == $hashToCompare;
    }
}



?>